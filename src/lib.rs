//! Yet another BMP280 crate
//!
//! Datasheet: https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bmp280-ds001.pdf
//! Vendor Reference Software: https://github.com/BoschSensortec/BMP2-Sensor-API
#![no_std]

const BMP2_MIN_PRES_32INT: u32 = 30000;
const BMP2_MAX_PRES_32INT: u32 = 110000;
const BMP2_MIN_PRES_64INT: u64 = 30000 * 256;
const BMP2_MAX_PRES_64INT: u64 = 110000 * 256;

const BMP2_MIN_TEMP_INT: i32 = -4000;
const BMP2_MAX_TEMP_INT: i32 = 8500;

struct CalibrationParameters {
    //Calibration parameter of temperature data
    /// Calibration t1 data
    pub dig_t1: u16,

    /// Calibration t2 data
    pub dig_t2: i16,

    /// Calibration t3 data
    pub dig_t3: i16,

    // Calibration parameter of pressure data
    /// Calibration p1 data
    pub dig_p1: u16,

    /// Calibration p2 data
    pub dig_p2: i16,

    /// Calibration p3 data
    pub dig_p3: i16,

    /// Calibration p4 data
    pub dig_p4: i16,

    /// Calibration p5 data
    pub dig_p5: i16,

    /// Calibration p6 data
    pub dig_p6: i16,

    /// Calibration p7 data
    pub dig_p7: i16,

    /// Calibration p8 data
    pub dig_p8: i16,

    /// Calibration p9 data
    pub dig_p9: i16,

    /// Calibration p10 data
    pub dig_p10: i8,

    /// Fine resolution temperature value
    pub t_fine: i32,
}

struct UncomponsatedData {
    pub temperature: i32,
    pub pressure: u32,
}

pub enum Error<T> {
    DivideByZero,
    ClampedMax(T),
    ClampedMin(T),
}

fn compensate_pressure_u32(
    uncompensated_data: &UncomponsatedData,
    calib_param: &CalibrationParameters,
) -> Result<u32, Error<u32>> {
    let mut var1: i32 = calib_param.t_fine / 2 - 64000;
    let mut var2: i32 = (((var1 / 4) * (var1 / 4)) / 2048) * (calib_param.dig_p6 as i32);
    var2 += (var1 * (calib_param.dig_p5 as i32)) * 2;
    var2 = (var2 / 4) + ((calib_param.dig_p4 as i32) * 65536);
    var1 = ((((calib_param.dig_p3 as i32) * (((var1 / 4) * (var1 / 4)) / 8192)) / 8)
        + (((calib_param.dig_p2 as i32) * var1) / 2))
        / 262144;
    var1 = ((32768 + var1) * (calib_param.dig_p1 as i32)) / 32768;

    // Avoid exception caused by division with zero
    if var1 == 0 {
        return Err(Error::DivideByZero);
    }

    let mut pressure: u32 = (1048576 - uncompensated_data.pressure) - ((var2 / 4096) as u32) * 3125;

    // Check for overflows against UINT32_MAX/2; if pressure is left-shifted by 1
    if pressure < u32::MAX / 2 {
        pressure = (pressure * 2) / (var1 as u32)
    } else {
        pressure = (pressure / (var1 as u32)) * 2
    }

    var1 = ((calib_param.dig_p9 as i32)
        * ((((pressure / 8) as i32) * (pressure / 8) as i32) / 8192))
        / 4096;
    var2 = (((pressure / 4) as i32) * (calib_param.dig_p8 as i32)) / 8192;

    pressure += ((var1 + var2 + calib_param.dig_p7 as i32) / 16) as u32;

    if pressure < BMP2_MIN_PRES_32INT {
        Err(Error::ClampedMin(BMP2_MIN_PRES_32INT))
    } else if pressure > BMP2_MAX_PRES_32INT {
        Err(Error::ClampedMax(BMP2_MAX_PRES_32INT))
    } else {
        Ok(pressure)
    }
}

fn compensate_temperature_i32(
    uncomp_data: &UncomponsatedData,
    calib_param: &mut CalibrationParameters,
) -> Result<i32, Error<i32>> {
    let var1: i32 = (((uncomp_data.temperature / 8) - ((calib_param.dig_t1 as i32) * 2))
        * (calib_param.dig_t2 as i32))
        / 2048;
    let var2: i32 = (((((uncomp_data.temperature / 16) - (calib_param.dig_t1 as i32))
        * ((uncomp_data.temperature / 16) - (calib_param.dig_t1 as i32)))
        / 4096)
        * (calib_param.dig_t3 as i32))
        / 16384;

    calib_param.t_fine = var1 + var2;

    let temperature: i32 = (calib_param.t_fine * 5 + 128) / 256;

    if temperature < BMP2_MIN_TEMP_INT {
        Err(Error::ClampedMin(BMP2_MIN_TEMP_INT))
    } else if temperature > BMP2_MAX_TEMP_INT {
        Err(Error::ClampedMax(BMP2_MAX_TEMP_INT))
    } else {
        Ok(temperature)
    }
}
